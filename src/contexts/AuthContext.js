import React, { createContext, useReducer, useState } from 'react';
import {authReducer} from '../reducers/AuthReducers'
export const AuthContext = createContext();

const AuthContextProvider = ({ children }) => {
    // useState
    const [isAuthenticated, dispatch] = useReducer(authReducer, false)

    // function change
    // const toggleAuth = () => {
    //     setAuthenticated(!isAuthenticated);
    // }

    // Context data 
    const authContextData = {
        isAuthenticated,
        // toggleAuth
        dispatch
    }

    // Return Provider
    return (
        <AuthContext.Provider value={authContextData}>
            {children}
        </AuthContext.Provider>
    )
}

export default AuthContextProvider;
