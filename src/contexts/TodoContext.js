import React, { createContext, useEffect, useReducer } from 'react';
import { todoReducer } from '../reducers/TodoReducers';
import { GET_TODOS, SAVE_TODOS } from '../reducers/types';

export const TodoContext = createContext();

const TodoContextProvider = ({ children }) => {
    // useState
    const [todos, dispatch] = useReducer(todoReducer, [])

    useEffect(() => {
        dispatch({
            type: GET_TODOS,
            payload: null
        })
    }, [])

    useEffect(() => {
        dispatch({
            type: SAVE_TODOS,
            payload: { todos }
        }, [todos])
    })

    // Context data 
    const todoContextData = {
        todos,
        dispatch
    }

    // Return Provider
    return (
        <TodoContext.Provider value={todoContextData}>
            {children}
        </TodoContext.Provider>
    )
}

export default TodoContextProvider;
