import React, { useState, useContext } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { ThemeContext } from '../contexts/ThemeContext';
import { TodoContext } from '../contexts/TodoContext';
import { ADD_TODO } from '../reducers/types';

const TodoForm = () => {

    //load context Todo
    const { dispatch } = useContext(TodoContext)

    const [title, setTitle] = useState('')

    const onTitleChange = event => {
        setTitle(event.target.value)
    }

    const handleSubmit = event => {
        event.preventDefault()
        dispatch({
            type: ADD_TODO,
            payload: {
                todo: {
                    id: uuidv4(),
                    title
                }
            }
        })
        setTitle('')
    }

    //load context 
    const { theme } = useContext(ThemeContext);
    const { isLightTheme, light, dark } = theme;
    const style = isLightTheme ? light : dark;

    return (
        <form onSubmit={handleSubmit}>
            <input
                type='text'
                name='title'
                placeholder='Enter new todo'
                onChange={onTitleChange}
                value={title}
                required
            />
            <input style={style} type='submit' value='Add' />
        </form>
    )
}

export default TodoForm